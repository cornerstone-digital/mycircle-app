'use strict'

describe 'Directive: newVersionAlert', ->

  beforeEach module 'versionPoll'

  $scope = null
  $element = null
  versionCheckService = null

  beforeEach inject ($rootScope, $compile, _versionCheckService_) ->
    $scope = $rootScope.$new()

    versionCheckService = _versionCheckService_
    spyOn versionCheckService, 'startPolling'

    $element = '<new-version-alert></new-version-alert>'
    $element = $compile($element) $scope

    $scope.$digest()

  it 'should be converted to a button', ->
    expect($element).toBeA 'button'

  it 'should not be visible', ->
    expect($element).toBeHidden()

  it 'should start polling', ->
    expect(versionCheckService.startPolling).toHaveBeenCalled()

  describe 'when the version changes', ->

    beforeEach ->
      $scope.$apply ->
        $scope.$root.$broadcast 'version:changed'

    it 'becomes visible', ->
      expect($element).not.toBeHidden()

