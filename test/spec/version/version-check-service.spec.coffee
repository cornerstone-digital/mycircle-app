'use strict'

describe 'Service: versionCheckService', ->

  beforeEach module 'versionPoll'

  $httpBackend = null
  $timeout = null
  service = null
  listener = null

  beforeEach inject ($rootScope, _$httpBackend_, _$timeout_, versionCheckService) ->
    $httpBackend = _$httpBackend_
    $timeout = _$timeout_
    service = versionCheckService

    listener = jasmine.createSpy()
    $scope = $rootScope.$new()
    $scope.$on 'version:changed', listener

    $httpBackend.whenGET('api://api/information').respond 200,
      apiVersion: "3.2.0"
      buildDate: "25/04/2014 @ 08:47:15"
      commitId: "abc123"
      environment: "development"
      hostname: "jetty-test-1.test.amazon.mycircleinc.net"
      message: "Johnny 5 says 'I am alive!'"
      version: "3.2.0-SNAPSHOT"

  describe 'polling for version changes', ->

    timestamp = null

    beforeEach ->
      timestamp = 'Fri, 28 Mar 2014 11:33:00 GMT'

      $httpBackend.expectGET('/version.txt').respond 200, '1',
          'Last-Modified': timestamp

      $httpBackend.expectGET('/git-version.txt').respond 200, '1',
        'Last-Modified': timestamp

      service.startPolling()

#    afterEach ->
#      $httpBackend.verifyNoOutstandingExpectation()
#      $httpBackend.verifyNoOutstandingRequest()

    it 'initializes the version after polling the first time', ->
      expect(service.currentVersion()).toBeNull()
      expect(service.lastModified()).toBeNull()

      $httpBackend.flush()

      expect(service.currentVersion()).toBe '1'
      expect(service.lastModified()).toBe timestamp

    describe 'when the version is re-polled', ->

      beforeEach ->
        $httpBackend.flush()

        $httpBackend.expectGET '/version.txt', (headers) ->
          headers['If-Modified-Since'] is timestamp
        .respond 200, '1',
          'Last-Modified': timestamp

        $httpBackend.expectGET '/git-version.txt', (headers) ->
          headers['If-Modified-Since'] is timestamp
        .respond 200, '1',
          'Last-Modified': timestamp

        $timeout.flush()
        $httpBackend.flush()

      it 'version is not changed', ->
        expect(service.currentVersion()).toBe '1'
        expect(service.lastModified()).toBe timestamp

      it 'no event is triggered', ->
        expect(listener).not.toHaveBeenCalled()

      describe 'when the version changes', ->

        newTimestamp = null

        beforeEach ->
          newTimestamp = 'Fri, 28 Mar 2014 11:49:00 GMT'

          $httpBackend.expectGET '/version.txt', (headers) ->
            headers['If-Modified-Since'] is timestamp
          .respond 200, '2',
            'Last-Modified': newTimestamp

          $httpBackend.expectGET '/git-version.txt', (headers) ->
            headers['If-Modified-Since'] is timestamp
          .respond 200, '2',
            'Last-Modified': newTimestamp

          $timeout.flush()
          $httpBackend.flush()

        it 'version is changed', ->
          expect(service.currentVersion()).toBe '2'
          expect(service.lastModified()).toBe newTimestamp

        it 'an event is triggered', ->
          expect(listener).toHaveBeenCalled()
          expect(listener.mostRecentCall.args[1]).toBe('2')

    describe 'when version check returns an HTTP 304', ->

      beforeEach ->
        $httpBackend.flush()

        $httpBackend.expectGET '/version.txt', (headers) ->
          headers['If-Modified-Since'] is timestamp
        .respond 304

        $httpBackend.expectGET '/git-version.txt', (headers) ->
          headers['If-Modified-Since'] is timestamp
        .respond 304

        $timeout.flush()
        $httpBackend.flush()

        $httpBackend.expectGET '/version.txt', (headers) ->
          headers['If-Modified-Since'] is timestamp
        .respond 304

        $httpBackend.expectGET '/git-version.txt', (headers) ->
          headers['If-Modified-Since'] is timestamp
        .respond 304

      it 'the service polls again', ->
        $timeout.flush()
        $httpBackend.flush()
